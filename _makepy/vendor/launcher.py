r"""
Bootstraps a makepy installation by:

- Creating a virtual environment,
- Installing ``makepy`` via the ``.whl`` next to this file -- this also downloads and installs ``makepy``'s dependencies.

The bootstrapping process is cached.

All command-line arguments are then forwarded to the virtual environment's ``python`` executable, with only
one twist: if the first argument is ``--make``, then the second argument is expected to be a path relative
to ``MAKEPY_DIR`` such that the following Batch commands are equivalent::

    "%~dp0\..\_makepy\vendor\launcher "%~dp0\..\_makepy\make.py" %*
    "%~dp0\..\_makepy\vendor\launcher --make make.py %*
"""
from __future__ import annotations
import os
from pathlib import Path
import shlex
import subprocess
import sys
from typing import Any, List, Mapping, Optional, Tuple, Union

MINIMUM_PYTHON_VERSION = (3, 8)
VENDOR_DIR = Path(__file__).parent
MAKEPY_DIR = VENDOR_DIR.parent
VENV_DIR = VENDOR_DIR / ".venv"
VENV_PYTHON = VENV_DIR / ("Scripts/python.exe" if sys.platform == "win32" else "bin/python")
MAKEPY_WHEEL_PATH = next(VENDOR_DIR.glob("makepy-*"))
_, MAKEPY_WHEEL_VERSION, _ = MAKEPY_WHEEL_PATH.name.split("-", 2)


def log(msg: str) -> None:
    print(f"[launcher] {msg}", file=sys.stderr)


def error(msg: str) -> None:
    log(f"ERROR: {msg}")


def run_command(
    cmd: List[Any],
    *,
    check: bool = True,
    capture_output: bool = False,
    env: Optional[Mapping[str, Union[str, Path]]] = None,
) -> subprocess.CompletedProcess[str]:
    env_ = dict(os.environ, **{k: str(v) for k, v in (env or {}).items()})
    cmd = list(map(str, cmd))
    log(f"[cmd] {shlex.join(cmd)}")

    return subprocess.run(cmd, env=env_, check=check, text=True, capture_output=capture_output)


def check_python_version() -> None:
    def fmtver(v: Tuple[int, int]) -> str:
        return ".".join(map(str, v))

    version = sys.version_info[:2]
    if version < MINIMUM_PYTHON_VERSION:
        error(f"Python >= {fmtver(MINIMUM_PYTHON_VERSION)} is required. Got: {fmtver(version)}")
        sys.exit(1)


def ensure_venv() -> None:
    if VENV_PYTHON.is_file():
        expected_version = MAKEPY_WHEEL_VERSION
        proc = run_command(
            [VENV_PYTHON, "-c", "import makepy; print(makepy.__version__)"], check=False, capture_output=True
        )
        assert proc.stdout is not None
        if proc.returncode != 0:
            log("Failed to infer version. Re-installing makepy.")
        else:
            version = proc.stdout.strip()
            if version == expected_version:
                return
            else:
                log(f"Expected makepy version '{expected_version}', got '{version}'.")

    log(f"Creating venv at: {VENV_DIR}")
    run_command([sys.executable, "-m", "venv", "--clear", VENV_DIR])
    log("Installing dependencies")
    run_command([VENV_PYTHON, "-m", "pip", "install", "-U", "pip"])
    run_command([VENV_PYTHON, "-m", "pip", "install", f"{MAKEPY_WHEEL_PATH}[prod]"])


def process_args(args: List[str]) -> List[str]:
    if len(args) < 2:
        return args
    first_arg, second_arg, *rest = args
    if first_arg != "--make":
        return args
    return [str(MAKEPY_DIR / second_arg)] + rest


def main() -> None:
    check_python_version()

    ensure_venv()

    args = process_args(sys.argv[1:])
    proc = run_command([str(VENV_PYTHON)] + args, check=False)
    sys.exit(proc.returncode)


if __name__ == "__main__":
    main()
