import datetime
import os
from pathlib import Path
import subprocess
import sys
from typing import List

# Copied from makepy.utils
PATHEXT: List[str]
if sys.platform == "win32":
    PATHEXT = os.environ.get("PATHEXT", ".bat").split(os.pathsep)
else:
    PATHEXT = []


def glob_pathext(exe: Path) -> Path:
    """
    Replicates Windows' automatic PATHEXT expansion -- this is what lets you omit the extension, e.g. "make" instead of "make.bat".

    The goal is to enable running ``.../trace .../make ...``.
    """
    if not exe.suffix:
        for candidate in [exe.with_suffix(ext) for ext in PATHEXT]:
            if candidate.is_file():
                return candidate
    if exe.is_file():
        return exe
    raise FileNotFoundError(exe)


def resolve_exe(exe: str) -> str:
    # NOTE: Casting to Path and back to str automatically normalizes slashes, which is what we want.
    return str(glob_pathext(Path(exe)).resolve())


def main() -> int:
    exe, *args = sys.argv[1:]
    exe = resolve_exe(exe)
    # NOTE: read bytes (no text=True) to not deal with encoding errors. We're just interested in prepending text.
    with subprocess.Popen([exe] + args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT) as proc:
        assert proc.stdout is not None
        for line in iter(proc.stdout.readline, b""):
            now = datetime.datetime.now().strftime("%H:%M:%S")
            sys.stdout.buffer.write(f"[{now}] ".encode("ascii") + line)
    return proc.returncode


if __name__ == "__main__":
    sys.exit(main())
