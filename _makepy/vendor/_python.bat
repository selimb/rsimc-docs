@echo off
setlocal
if "%PYTHON%" == "" echo ERROR: PYTHON is undefined & exit /b 1
if not exist "%PYTHON%" echo ERROR: No file found at PYTHON (%PYTHON%) & exit /b 1
"%PYTHON%" %*