# Style Guide
# ===========
#
# - Use snake_case for functions, ALL_CAPS for values (which should all be constants).
# - Except for CI_DEF, all functions/values should be prefixed with underscore, this way vscode (pylance) can mark them as
#   unused in this file.
from pathlib import Path

from makepy.ext import gitlab

from common import REPO_DIR
from make import Docs

_trace = gitlab.TracedScript("_makepy/vendor/trace")


def _make(args: str) -> str:
    return _trace(f"./make {args}")


def _path2str(p: Path) -> str:
    return p.relative_to(REPO_DIR).as_posix()


_BEFORE_SCRIPT = gitlab.before_script(_make("install-libs"))

CI_DEF = gitlab.build_def(
    variables={"PYTHON": "python3.8", "PIP_CACHE_DIR": "$CI_PROJECT_DIR/.cache/pip"},
    stages={
        "ci_yml": {
            "ci_yml": gitlab.job(
                gitlab.script(_make("gitlab-yml --check")),
            )
        },
        "lint": {
            "lint": gitlab.job(
                _BEFORE_SCRIPT,
                gitlab.script(
                    _trace("_makepy/make lint"),
                    _make("lint-py"),
                ),
            )
        },
        "docs": {
            "docs": gitlab.job(
                _BEFORE_SCRIPT,
                gitlab.script(
                    _make("docs"),
                ),
                gitlab.artifacts(
                    [_path2str(Docs.BUILD_DIR)],
                    expose_as="docs",
                ),
            ),
        },
    },
)
CI_DEF.update(  # TODO makepy: Allow passing extra arguments to build_def
    {
        "image": "python:3.8",
        "cache": {
            "paths": [".cache/pip"],
        },
    }
)

# # Useful to test a specific job
# JOB_FILTER = ["frontend_test"]
# for key, value in list(CI_DEF.items()):
#     if key in JOB_FILTER:
#         value.pop("needs", None)
#     elif key not in ("stages", "variables", "ci_yml"):
#         CI_DEF.pop(key)
