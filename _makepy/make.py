from pathlib import Path
import sys

import makepy
from makepy import log
import makepy.ext.gitlab
import makepy.ext.python
import makepy.ext.vscode
from makepy.utils import glob_pathext, glob_rel

from common import REPO_DIR


class PythonContext(makepy.ext.python.PythonContext):
    def get_compiled_reqs_file(self, requirement: makepy.ext.python.RequirementGroup) -> Path:
        if requirement == "dev":
            # For netlify
            return REPO_DIR / "requirements.txt"
        else:
            return super().get_compiled_reqs_file(requirement)


context = PythonContext(
    project_dir=REPO_DIR,
    lint_paths=glob_rel(REPO_DIR, "docs/source/*.py"),
    first_party=[],
    coverage_packages=[],
    requirement_groups=["dev"],
    default_requirement_group="dev",
)


def rel_repo(p: Path) -> Path:
    return p.relative_to(REPO_DIR)


class LintPy(makepy.ext.python.Lint):
    name = "lint-py"


class FormatPy(makepy.ext.python.Format):
    name = "format-py"


class Edit(makepy.ext.vscode.Edit):
    path_arg = REPO_DIR


class GenerateGitlabYaml(makepy.ext.gitlab.GenerateGitlabYaml):
    repo_dir = REPO_DIR

    @property
    def data(self) -> makepy.ext.gitlab.DictStrAny:
        from gitlab_ci_yml import CI_DEF

        return CI_DEF


class Docs(makepy.Command):
    context: makepy.ext.python.PythonContext
    DOCS_DIR = REPO_DIR / "docs"
    SOURCE_DIR = DOCS_DIR / "source"
    BUILDER = "html"
    BUILD_DIR = DOCS_DIR / BUILDER

    @makepy.command("docs")
    def run(self) -> None:
        """Build the docs."""
        venv_python = self.context.ensure_venv()
        sphinx_build_exe = venv_python.parent / "sphinx-build"
        try:
            glob_pathext(sphinx_build_exe)
        except FileNotFoundError:
            log.error(
                f"Could not find sphinx executable at '{sphinx_build_exe}'. Run 'make install-libs' to install sphinx."
            )
            sys.exit(1)

        self.context.run_command(
            [
                sphinx_build_exe,
                rel_repo(self.SOURCE_DIR),
                rel_repo(self.BUILD_DIR),
                "-b",
                self.BUILDER,
                "-W",
                "--keep-going",
            ]
        )


cli = makepy.create_cli(
    context,
    commands=[
        makepy.ext.python.Python,
        makepy.ext.python.FreezeLibs,
        makepy.ext.python.InstallLibs,
        LintPy,
        FormatPy,
        Edit,
        GenerateGitlabYaml,
        Docs,
    ],
)

if __name__ == "__main__":
    cli()
