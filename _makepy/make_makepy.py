from pathlib import Path

import makepy
import makepy.ext.python
from makepy.utils import glob_rel, run_command

from common import MAKEPY_DIR, REPO_DIR

PROJECT_DIR = MAKEPY_DIR


class PythonContext(makepy.ext.python.PythonContext):
    def ensure_venv(self) -> Path:
        return makepy.ext.python.MAKEPY_PYTHON


make_files = glob_rel(MAKEPY_DIR, "*.py")
context = PythonContext(
    project_dir=PROJECT_DIR,
    lint_paths=make_files,
    first_party=[Path(p).stem for p in make_files],
    coverage_packages=[],
)


class Chmod(makepy.Command):
    @staticmethod
    @makepy.command("chmod")
    def run() -> None:
        """Set ``chmod +x`` on all git-tracked files that have a sh/bash shebang line."""
        result = run_command(
            [
                "git",
                "grep",
                "--name-only",
                # Don't match binary files
                "-I",
                "^#!/",
            ],
            capture_output=True,
            cwd=REPO_DIR,
        )
        for line in result.stdout.splitlines():
            # paths are relative to REPO_DIR
            relpath = line.strip()
            if not relpath:
                continue

            run_command(["git", "update-index", "--chmod=+x", relpath], cwd=REPO_DIR)


cli = makepy.create_cli(
    context,
    commands=[
        makepy.ext.python.Lint,
        makepy.ext.python.Format,
        makepy.ext.python.Python,
        Chmod,
    ],
)

if __name__ == "__main__":
    cli()
