=======
Runners
=======

The web portal page named **Runners** lists the approved Remote Simulation runners.

For each runner, it indicates the supported solvers, its status, its downtime or uptime, queue type, and number of processors (CPUs).

**On this page, you can:**
    - Sort the table based on runner name or number of processors, and filter it based on status, solver type, or queue type.
    - Navigate to a runner information page.
    - Navigate through the runners table.