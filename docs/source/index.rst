.. Remote Simulation documentation master file, created by
   sphinx-quickstart on Tue Nov 16 14:55:12 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=======================================
Welcome to Remote Simulation web portal
=======================================

Use the Remote Simulation web portal to solve remotely on another, more powerful, machine or on a cluster, called Remote Simulation *runners* a solution that you defined in Simcenter 3D and uploaded to the Remote Simulation server from which it is sent to runners for solving.

From the Remote Simulation web portal, you can track the solver logs and graphs while the solutions are solving.

.. raw:: html

   <video width="100%" controls>
      <source src="_static/getting-started.mp4" type="video/mp4">
      Your browser does not support the video tag.
   </video>

.. toctree::
   :hidden:
   :maxdepth: 4

   user_interface
   architecture



