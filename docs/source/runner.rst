==================
Runner information
==================

The web portal page for a runner displays runner information, number of active solves, and its solve history. The page name contains the runner name and status.

**On this page, you can:**
    - Launch a solve.
    - Navigate to a solve information page.
    - Navigate through the current runner's table.