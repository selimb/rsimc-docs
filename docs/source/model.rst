=================
Model information
=================

The web portal page for a model displays the model information and its solve history. The page name is the model name.

**On this page, you can:**
    - Download or delete the model.
    - Launch a solve of the current model.
    - Navigate to a solve information page.
    - Navigate through the current model's table.