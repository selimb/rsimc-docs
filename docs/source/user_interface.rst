=========================
Web portal user interface
=========================

.. image:: images/remote_solution_web_ui.png

The web protal user interface contains the following components:   

\(1) Location
   Indicates the current web portal page name.
   
      - For a solve information page, the page name contains the solve's ID number and status.
      - For a model information page, the page name is the model name.
      - For a runner information page, the page name contains the runner name and status.

\(2) Navigation commands
   Lets you navigate the Remote Simulation web portal.
   The following commands are available when you are not logged in:

      |login| **Login**—Lets you log into the Remote Simulation web portal.

      |register| **Register**—Lets you create a new account for Remote Simulation.

   The following commands are available when you are logged in:

      |home| **Home**—Opens the **Home** page that lists current and recent solves, and latest models.

      |solves| **Solves**—Opens the **Solves** page that lists the solves with their information.

      |models| **Models**—Opens the **Models** page that lists the models with their information.

      |runners| **Runners**—Opens the **Runners** page that lists the runners with their information.

\(3) Main window
   Contains different information depending on the page.

\(4) User indicator and log-out icon
   Displays the first letter of the user that is logged in. When you hover over it, it indicates the username.

      |logout| **Log out**—Disconnects you from the web portal.


*Note*: Users can view only their own models and solves. If you are the administrator, you can view the solves and models of all users, not just your own, and you can sort tables by owner.

The web portal has multiple types of pages when you are log into the web portal. These are:

.. toctree::
   
   home
   solves
   solve
   models
   model
   runners
   runner

.. |login| image:: images/login.png
.. |register| image:: images/register.png
.. |home| image:: images/home.png
.. |solves| image:: images/solves.png
.. |models| image:: images/models.png
.. |runners| image:: images/runners.png
.. |logout| image:: images/logout.png