======
Solves
======

The web portal page named **Solves** lists the solves history for all your solves, and for all solves if you are the administrator.

For each solve, it indicates its model name, the runner on which it was executed, its status, when it was last modified, and the availability of results.

**On this page, you can:**
    - Launch a new solve, or solve the selected model.
    - Navigate to the Solves, solve information, Models, or model information page.
    - Upload a previously downloaded model.