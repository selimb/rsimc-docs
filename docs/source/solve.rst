=================
Solve information
=================

The web portal page for a solve displays the solve data, logs, and result graphs. They are updated in real-time as the solve executes. The page name contains the solve's ID number and status.


**On this page, you can:**
    - Navigate to the model or runner information page.
    - Show or hide solve parameters and the description.
    - Download the results when available.
    - Execute the solve again or cancel it while it is running.
    - Monitor the solver and runner logs, or inspect a selected graph while the solve is running or afterward.
    - Copy the log information to clipboard by clicking **Copy**.