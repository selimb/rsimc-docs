==============================
Remote Simulation architecture
==============================

.. image:: images/remote_simulation_arch.png

**Remote Simulation server**
    Is a dedicated server that runs services, such as the web server for accessing the web portal, a database, which stores the user models and results, a service for downloading/uploading model archives and solved results.
    The server communicates with all other components and passes information from the web portal or the Remote Simulation client in Simcenter 3D to the runners and vice versa.

**Remote Simulation runners**
    Are the remote computation hosts, which receive simulations jobs, solve them, and keep track of their progress while they are solving.
    Each host communicates with the Remote Simulation server.
    The computation host can be a Windows or Linux machine, or a cluster with the SGE scheduler.

    The following solvers are supported:

        - Simcenter Nastran
        - The thermal/flow solver in the Simcenter 3D Thermal/Flow, Space Systems Thermal, and Electronic Systems Cooling environments
        - Simcenter 3D Multiphysics

    The runners with the SGE scheduler queue type support only the thermal/flow solver.

**Remote Simulation web portal**
    Lets you upload and solve models, display information about solves, models, and runners, and track solutions while they are solving from a browser.
    You need a user account to access the web portal. The web portal communicates with the Remote Simulation server.

**Remote Simulation client in Simcenter 3D**
    Lets you upload and solve models, and download results from solved models directly in Simcenter 3D using the Remote Simulation command.
    You need to be authenticated using the same username and password combination as for the web portal.
    The Remote Simulation client in Simcenter 3D communicates with the Remote Simulation server.