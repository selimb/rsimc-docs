======
Models
======

The web portal page named **Models** lists the models history for all your models, and for all models if you are the administrator.

For each model, it indicates the date it was uploaded and the size of the model on the disk.

**On this page, you can:**
    - Sort the table based on the model name, uploaded date, or size on disk.
    - Upload a previously downloaded model.
    - Navigate to a model information page.
    - Navigate through the models table.