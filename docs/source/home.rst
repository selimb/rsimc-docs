====
Home
====

The web portal page named **Home** lists up to five current and recent solves, and latest models in their respective tables.

For the solves, it indicates their model name, the runner on which they are executed, their status, and when they were last modified.

For the models, it indicates their name and the date they were uploaded to the server.

**On this page, you can:**
    - Launch a new solve, or solve the selected model.
    - Navigate to the Solves, solve information, Models, or model information page.
    - Upload a previously downloaded model.