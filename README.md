# Remote Simulation Docs

[![Netlify Status](https://api.netlify.com/api/v1/badges/a8083754-5905-4535-be57-2f9436b2a47e/deploy-status)](https://app.netlify.com/sites/rsimc/deploys)

## Prerequisites

1. Python 3.8
   - You can grab the one from http://vm-gitdcim.pc.mayahtt.ca/hackathon-pathfinders/rsimc-docs/-/packages/1.
     Extract it anywhere.
2. Visual Studio Code (Optional, but highly recommended)
   - You can download it from https://code.visualstudio.com/ and install it without admin privileges.
     Make sure to check the "Add to PATH" option.
   - You'll also need these extensions for a better experience. You should be able to install them from the website if you have Visual Studio Code installed.
     - [reStructuredText](https://marketplace.visualstudio.com/items?itemName=lextudio.restructuredtext)
     - [reStructuredText Syntax highlighting](https://marketplace.visualstudio.com/items?itemName=trond-snekvik.simple-rst)
     - [Python](https://marketplace.visualstudio.com/items?itemName=ms-python.python)
3. [Git Bash](https://git-scm.com/downloads)
   - You need to ask IT for that one -- they're used to installing it.
     Make sure they select the option to add it to PATH such that you can run `git` from any shell.
   - Make sure that running `where git` from a command prompt shows something like `C:\Program Files\Git\cmd\git.exe` _first_, otherwise you'll need to tweak your PATH environment variable.

## Setup

- Open a command prompt, `cd` to the directory where you want this project to live, and clone the repo using the command below.
  This will create a `rsimc-docs` directory under your current directory, and `cd` into it.

```
git clone https://gitrepo.mayahtt.com/hackathon-pathfinders/rsimc-docs.git
cd rsimc-docs
```

If prompted for a username and password when running `git clone`, enter your Gitlab credentials, i.e. your Maya credentials.

- Copy `setenv.bat.template` to `setenv.bat`, and ensure the value for `PYTHON` matches your machine's.
- Call `setenv.bat` from your command prompt.

You're now ready to run this project's `make` commands.

- Run `make install-libs` to install Sphinx and other dependencies.
- Run `make edit` to open the project in Visual Studio Code.
- Run `make docs` to build the documentation from the command-line.
  Alternatively, you can build it from Visual Studio Code by hitting "Ctrl+Shift+p" and running "reStructuredText: Open Preview to the Side" -- the default keyboard shortcut is Ctrl+K+R.

## Git Workflow

Changes that you make aren't automatically sync'd with Gitlab.
You'll need to use Git (either from the command-line, from TortoiseGit, or from Visual Studio Code) to push changes.
First, configure your username and email if you haven't already, as shown below -- make sure to change `Selim Belhaouane` and `selim.belhaouane@mayahtt.com` to your own name and Maya email.

```
git config --global user.name "Selim Belhaouane"
git config --global user.email "selim.belhaouane@mayahtt.com"
```

You'll then usually work on a "branch", which you'll then request to merge to the main branch (`dev`) with a [merge request](http://vm-gitdcim.pc.mayahtt.ca/hackathon-pathfinders/rsimc-docs/-/merge_requests).
A sample workflow looks like this:

- Make sure you're on the `dev` branch -- you can use `git status` or `git branch` to verify.
- Run `git checkout -b my-new-branch`

You can also automatically create a branch and merge request from an issue in Gitlab.
Try with #1 !

- Make some changes to `docs/source/test.rst`, for instance.
- Run `git status` and/or `git diff` to review the changes.
- Run `git add docs/source/test.rst` and `git commit -m "some commit message"` to commit the changes.
- Run `git push` to push the changes.

## ReStructuredText Reference

- https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html
- https://docutils.sourceforge.io/docs/user/rst/quickref.html

`docutils` defines the reStructuredText syntax and implements common elements (lists, bold, italic, etc...).
`sphinx` builds on `docutils`, and provides more elements.
It's also possible to define your own elements (through custom directives and roles implemented in Python), which is where reStructuredText shines.
