#!/bin/bash
set -u -e
HERE="$(dirname "$(readlink -fm "$0")")"
"$HERE/_makepy/vendor/launcher" --make make.py "$@"
